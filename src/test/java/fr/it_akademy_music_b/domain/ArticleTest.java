package fr.it_akademy_music_b.domain;

import static fr.it_akademy_music_b.domain.ArticleTestSamples.*;
import static fr.it_akademy_music_b.domain.CategoryTestSamples.*;
import static fr.it_akademy_music_b.domain.CommentTestSamples.*;
import static fr.it_akademy_music_b.domain.GenreTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import fr.it_akademy_music_b.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ArticleTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Article.class);
        Article article1 = getArticleSample1();
        Article article2 = new Article();
        assertThat(article1).isNotEqualTo(article2);

        article2.setId(article1.getId());
        assertThat(article1).isEqualTo(article2);

        article2 = getArticleSample2();
        assertThat(article1).isNotEqualTo(article2);
    }

    @Test
    void categoriesTest() throws Exception {
        Article article = getArticleRandomSampleGenerator();
        Category categoryBack = getCategoryRandomSampleGenerator();

        article.addCategories(categoryBack);
        assertThat(article.getCategories()).containsOnly(categoryBack);
        assertThat(categoryBack.getArticle()).isEqualTo(article);

        article.removeCategories(categoryBack);
        assertThat(article.getCategories()).doesNotContain(categoryBack);
        assertThat(categoryBack.getArticle()).isNull();

        article.categories(new HashSet<>(Set.of(categoryBack)));
        assertThat(article.getCategories()).containsOnly(categoryBack);
        assertThat(categoryBack.getArticle()).isEqualTo(article);

        article.setCategories(new HashSet<>());
        assertThat(article.getCategories()).doesNotContain(categoryBack);
        assertThat(categoryBack.getArticle()).isNull();
    }

    @Test
    void genresTest() throws Exception {
        Article article = getArticleRandomSampleGenerator();
        Genre genreBack = getGenreRandomSampleGenerator();

        article.addGenres(genreBack);
        assertThat(article.getGenres()).containsOnly(genreBack);
        assertThat(genreBack.getArticle()).isEqualTo(article);

        article.removeGenres(genreBack);
        assertThat(article.getGenres()).doesNotContain(genreBack);
        assertThat(genreBack.getArticle()).isNull();

        article.genres(new HashSet<>(Set.of(genreBack)));
        assertThat(article.getGenres()).containsOnly(genreBack);
        assertThat(genreBack.getArticle()).isEqualTo(article);

        article.setGenres(new HashSet<>());
        assertThat(article.getGenres()).doesNotContain(genreBack);
        assertThat(genreBack.getArticle()).isNull();
    }

    @Test
    void commentsTest() throws Exception {
        Article article = getArticleRandomSampleGenerator();
        Comment commentBack = getCommentRandomSampleGenerator();

        article.addComments(commentBack);
        assertThat(article.getComments()).containsOnly(commentBack);
        assertThat(commentBack.getArticle()).isEqualTo(article);

        article.removeComments(commentBack);
        assertThat(article.getComments()).doesNotContain(commentBack);
        assertThat(commentBack.getArticle()).isNull();

        article.comments(new HashSet<>(Set.of(commentBack)));
        assertThat(article.getComments()).containsOnly(commentBack);
        assertThat(commentBack.getArticle()).isEqualTo(article);

        article.setComments(new HashSet<>());
        assertThat(article.getComments()).doesNotContain(commentBack);
        assertThat(commentBack.getArticle()).isNull();
    }
}
