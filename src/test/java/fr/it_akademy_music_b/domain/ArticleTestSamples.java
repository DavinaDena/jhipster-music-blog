package fr.it_akademy_music_b.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class ArticleTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Article getArticleSample1() {
        return new Article().id(1L).title("title1").article("article1");
    }

    public static Article getArticleSample2() {
        return new Article().id(2L).title("title2").article("article2");
    }

    public static Article getArticleRandomSampleGenerator() {
        return new Article().id(longCount.incrementAndGet()).title(UUID.randomUUID().toString()).article(UUID.randomUUID().toString());
    }
}
