package fr.it_akademy_music_b.domain;

import static fr.it_akademy_music_b.domain.ArticleTestSamples.*;
import static fr.it_akademy_music_b.domain.CategoryTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import fr.it_akademy_music_b.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CategoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Category.class);
        Category category1 = getCategorySample1();
        Category category2 = new Category();
        assertThat(category1).isNotEqualTo(category2);

        category2.setId(category1.getId());
        assertThat(category1).isEqualTo(category2);

        category2 = getCategorySample2();
        assertThat(category1).isNotEqualTo(category2);
    }

    @Test
    void articleTest() throws Exception {
        Category category = getCategoryRandomSampleGenerator();
        Article articleBack = getArticleRandomSampleGenerator();

        category.setArticle(articleBack);
        assertThat(category.getArticle()).isEqualTo(articleBack);

        category.article(null);
        assertThat(category.getArticle()).isNull();
    }
}
