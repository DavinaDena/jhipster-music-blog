package fr.it_akademy_music_b.domain;

import static fr.it_akademy_music_b.domain.ArticleTestSamples.*;
import static fr.it_akademy_music_b.domain.GenreTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import fr.it_akademy_music_b.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GenreTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Genre.class);
        Genre genre1 = getGenreSample1();
        Genre genre2 = new Genre();
        assertThat(genre1).isNotEqualTo(genre2);

        genre2.setId(genre1.getId());
        assertThat(genre1).isEqualTo(genre2);

        genre2 = getGenreSample2();
        assertThat(genre1).isNotEqualTo(genre2);
    }

    @Test
    void articleTest() throws Exception {
        Genre genre = getGenreRandomSampleGenerator();
        Article articleBack = getArticleRandomSampleGenerator();

        genre.setArticle(articleBack);
        assertThat(genre.getArticle()).isEqualTo(articleBack);

        genre.article(null);
        assertThat(genre.getArticle()).isNull();
    }
}
