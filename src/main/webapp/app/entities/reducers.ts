import article from 'app/entities/article/article.reducer';
import genre from 'app/entities/genre/genre.reducer';
import category from 'app/entities/category/category.reducer';
import comment from 'app/entities/comment/comment.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  article,
  genre,
  category,
  comment,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
