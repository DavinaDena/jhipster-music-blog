import { ICategory } from 'app/shared/model/category.model';
import { IGenre } from 'app/shared/model/genre.model';
import { IComment } from 'app/shared/model/comment.model';

export interface IArticle {
  id?: number;
  title?: string | null;
  article?: string | null;
  categories?: ICategory[] | null;
  genres?: IGenre[] | null;
  comments?: IComment[] | null;
}

export const defaultValue: Readonly<IArticle> = {};
