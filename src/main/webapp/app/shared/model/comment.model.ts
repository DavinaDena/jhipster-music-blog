import { IArticle } from 'app/shared/model/article.model';

export interface IComment {
  id?: number;
  comment?: string | null;
  article?: IArticle | null;
}

export const defaultValue: Readonly<IComment> = {};
