import { IArticle } from 'app/shared/model/article.model';

export interface ICategory {
  id?: number;
  category?: string | null;
  article?: IArticle | null;
}

export const defaultValue: Readonly<ICategory> = {};
