import { IArticle } from 'app/shared/model/article.model';

export interface IGenre {
  id?: number;
  genre?: string | null;
  article?: IArticle | null;
}

export const defaultValue: Readonly<IGenre> = {};
