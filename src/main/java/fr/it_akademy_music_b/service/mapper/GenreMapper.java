package fr.it_akademy_music_b.service.mapper;

import fr.it_akademy_music_b.domain.Article;
import fr.it_akademy_music_b.domain.Genre;
import fr.it_akademy_music_b.service.dto.ArticleDTO;
import fr.it_akademy_music_b.service.dto.GenreDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Genre} and its DTO {@link GenreDTO}.
 */
@Mapper(componentModel = "spring")
public interface GenreMapper extends EntityMapper<GenreDTO, Genre> {
    @Mapping(target = "article", source = "article", qualifiedByName = "articleId")
    GenreDTO toDto(Genre s);

    @Named("articleId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ArticleDTO toDtoArticleId(Article article);
}
