package fr.it_akademy_music_b.service.mapper;

import fr.it_akademy_music_b.domain.Article;
import fr.it_akademy_music_b.domain.Category;
import fr.it_akademy_music_b.service.dto.ArticleDTO;
import fr.it_akademy_music_b.service.dto.CategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Category} and its DTO {@link CategoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {
    @Mapping(target = "article", source = "article", qualifiedByName = "articleId")
    CategoryDTO toDto(Category s);

    @Named("articleId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ArticleDTO toDtoArticleId(Article article);
}
