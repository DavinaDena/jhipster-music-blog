package fr.it_akademy_music_b.service.mapper;

import fr.it_akademy_music_b.domain.Article;
import fr.it_akademy_music_b.service.dto.ArticleDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Article} and its DTO {@link ArticleDTO}.
 */
@Mapper(componentModel = "spring")
public interface ArticleMapper extends EntityMapper<ArticleDTO, Article> {}
