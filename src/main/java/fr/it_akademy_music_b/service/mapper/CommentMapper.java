package fr.it_akademy_music_b.service.mapper;

import fr.it_akademy_music_b.domain.Article;
import fr.it_akademy_music_b.domain.Comment;
import fr.it_akademy_music_b.service.dto.ArticleDTO;
import fr.it_akademy_music_b.service.dto.CommentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Comment} and its DTO {@link CommentDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommentMapper extends EntityMapper<CommentDTO, Comment> {
    @Mapping(target = "article", source = "article", qualifiedByName = "articleId")
    CommentDTO toDto(Comment s);

    @Named("articleId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ArticleDTO toDtoArticleId(Article article);
}
