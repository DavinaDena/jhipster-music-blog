package fr.it_akademy_music_b.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Article.
 */
@Entity
@Table(name = "article")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "article")
    private String article;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "article" }, allowSetters = true)
    private Set<Category> categories = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "article" }, allowSetters = true)
    private Set<Genre> genres = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "article" }, allowSetters = true)
    private Set<Comment> comments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Article id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public Article title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArticle() {
        return this.article;
    }

    public Article article(String article) {
        this.setArticle(article);
        return this;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Set<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(Set<Category> categories) {
        if (this.categories != null) {
            this.categories.forEach(i -> i.setArticle(null));
        }
        if (categories != null) {
            categories.forEach(i -> i.setArticle(this));
        }
        this.categories = categories;
    }

    public Article categories(Set<Category> categories) {
        this.setCategories(categories);
        return this;
    }

    public Article addCategories(Category category) {
        this.categories.add(category);
        category.setArticle(this);
        return this;
    }

    public Article removeCategories(Category category) {
        this.categories.remove(category);
        category.setArticle(null);
        return this;
    }

    public Set<Genre> getGenres() {
        return this.genres;
    }

    public void setGenres(Set<Genre> genres) {
        if (this.genres != null) {
            this.genres.forEach(i -> i.setArticle(null));
        }
        if (genres != null) {
            genres.forEach(i -> i.setArticle(this));
        }
        this.genres = genres;
    }

    public Article genres(Set<Genre> genres) {
        this.setGenres(genres);
        return this;
    }

    public Article addGenres(Genre genre) {
        this.genres.add(genre);
        genre.setArticle(this);
        return this;
    }

    public Article removeGenres(Genre genre) {
        this.genres.remove(genre);
        genre.setArticle(null);
        return this;
    }

    public Set<Comment> getComments() {
        return this.comments;
    }

    public void setComments(Set<Comment> comments) {
        if (this.comments != null) {
            this.comments.forEach(i -> i.setArticle(null));
        }
        if (comments != null) {
            comments.forEach(i -> i.setArticle(this));
        }
        this.comments = comments;
    }

    public Article comments(Set<Comment> comments) {
        this.setComments(comments);
        return this;
    }

    public Article addComments(Comment comment) {
        this.comments.add(comment);
        comment.setArticle(this);
        return this;
    }

    public Article removeComments(Comment comment) {
        this.comments.remove(comment);
        comment.setArticle(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Article)) {
            return false;
        }
        return getId() != null && getId().equals(((Article) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Article{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", article='" + getArticle() + "'" +
            "}";
    }
}
